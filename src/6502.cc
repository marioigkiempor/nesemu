
#include "6502.h"

#include "defines.h"

static u8 fetch(CPU6502 &cpu) {
    if (cpu.instructions_lookup[cpu.current_opcode].address_mode != Address_Mode::IMP) {
        // Don't need to fetch for instructions where the address is implied.
        cpu.last_fetched = read(cpu, cpu.current_absolute_address);
    }

    return cpu.last_fetched;
}

void init(CPU6502 &cpu) {
    cpu.current_absolute_address = 0;
    cpu.current_relative_address = 0;
    cpu.current_opcode           = 0;

    // Initialise instruction lookup.
    auto &l = cpu.instructions_lookup;
    auto i = [] (const char *name, Opcode opcode, Address_Mode address_mode, u8 cycle_cost) {
        return CPU6502::Instruction{name, opcode, address_mode, cycle_cost};
    };

    l[0x00] = i("BRK", BRK, IMM, 7); l[0x01] = i("ORA", ORA, IZX, 6); l[0x02] = i("???", XXX, IMP, 2); l[0x03] = i("???", XXX, IMP, 8); l[0x04] = i("???", XXX, IMP, 3); l[0x05] = i("ORA", ORA, ZP0, 3); l[0x06] = i("ASL", ASL, ZP0, 5); l[0x07] = i("???", XXX, IMP, 5); l[0x08] = i("PHP", PHP, IMP, 3); l[0x09] = i("ORA", ORA, IMM, 2); l[0x0A] = i("ASL", ASL, IMP, 2); l[0x0B] = i("???", XXX, IMP, 2); l[0x0C] = i("???", XXX, IMP, 4); l[0x0D] = i("ORA", ORA, ABS, 4); l[0x0E] = i("ASL", ASL, ABS, 6); l[0x0F] = i("???", XXX, IMP, 6);
    l[0x10] = i("BPL", BPL, REL, 2); l[0x11] = i("ORA", ORA, IZY, 5); l[0x12] = i("???", XXX, IMP, 2); l[0x13] = i("???", XXX, IMP, 8); l[0x14] = i("???", XXX, IMP, 4); l[0x15] = i("ORA", ORA, ZPX, 4); l[0x16] = i("ASL", ASL, ZPX, 6); l[0x17] = i("???", XXX, IMP, 6); l[0x18] = i("CLC", CLC, IMP, 2); l[0x19] = i("ORA", ORA, ABY, 4); l[0x1A] = i("???", XXX, IMP, 2); l[0x1B] = i("???", XXX, IMP, 7); l[0x1C] = i("???", XXX, IMP, 4); l[0x1D] = i("ORA", ORA, ABX, 4); l[0x1E] = i("ASL", ASL, ABX, 7); l[0x1F] = i("???", XXX, IMP, 7);
    l[0x20] = i("JSR", JSR, ABS, 6); l[0x21] = i("AND", AND, IZX, 6); l[0x22] = i("???", XXX, IMP, 2); l[0x23] = i("???", XXX, IMP, 8); l[0x24] = i("BIT", BIT, ZP0, 3); l[0x25] = i("AND", AND, ZP0, 3); l[0x26] = i("ROL", ROL, ZP0, 5); l[0x27] = i("???", XXX, IMP, 5); l[0x28] = i("PLP", PLP, IMP, 4); l[0x29] = i("AND", AND, IMM, 2); l[0x2A] = i("ROL", ROL, IMP, 2); l[0x2B] = i("???", XXX, IMP, 2); l[0x2C] = i("BIT", BIT, ABS, 4); l[0x2D] = i("AND", AND, ABS, 4); l[0x2E] = i("ROL", ROL, ABS, 6); l[0x2F] = i("???", XXX, IMP, 6);
    l[0x30] = i("BMI", BMI, REL, 2); l[0x31] = i("AND", AND, IZY, 5); l[0x32] = i("???", XXX, IMP, 2); l[0x33] = i("???", XXX, IMP, 8); l[0x34] = i("???", XXX, IMP, 4); l[0x35] = i("AND", AND, ZPX, 4); l[0x36] = i("ROL", ROL, ZPX, 6); l[0x37] = i("???", XXX, IMP, 6); l[0x38] = i("SEC", SEC, IMP, 2); l[0x39] = i("AND", AND, ABY, 4); l[0x3A] = i("???", XXX, IMP, 2); l[0x3B] = i("???", XXX, IMP, 7); l[0x3C] = i("???", XXX, IMP, 4); l[0x3D] = i("AND", AND, ABX, 4); l[0x3E] = i("ROL", ROL, ABX, 7); l[0x3F] = i("???", XXX, IMP, 7);
    l[0x40] = i("RTI", RTI, IMP, 6); l[0x41] = i("EOR", EOR, IZX, 6); l[0x42] = i("???", XXX, IMP, 2); l[0x43] = i("???", XXX, IMP, 8); l[0x44] = i("???", XXX, IMP, 3); l[0x45] = i("EOR", EOR, ZP0, 3); l[0x46] = i("LSR", LSR, ZP0, 5); l[0x47] = i("???", XXX, IMP, 5); l[0x48] = i("PHA", PHA, IMP, 3); l[0x49] = i("EOR", EOR, IMM, 2); l[0x4A] = i("LSR", LSR, IMP, 2); l[0x4B] = i("???", XXX, IMP, 2); l[0x4C] = i("JMP", JMP, ABS, 3); l[0x4D] = i("EOR", EOR, ABS, 4); l[0x4E] = i("LSR", LSR, ABS, 6); l[0x4F] = i("???", XXX, IMP, 6);
    l[0x50] = i("BVC", BVC, REL, 2); l[0x51] = i("EOR", EOR, IZY, 5); l[0x52] = i("???", XXX, IMP, 2); l[0x53] = i("???", XXX, IMP, 8); l[0x54] = i("???", XXX, IMP, 4); l[0x55] = i("EOR", EOR, ZPX, 4); l[0x56] = i("LSR", LSR, ZPX, 6); l[0x57] = i("???", XXX, IMP, 6); l[0x58] = i("CLI", CLI, IMP, 2); l[0x59] = i("EOR", EOR, ABY, 4); l[0x5A] = i("???", XXX, IMP, 2); l[0x5B] = i("???", XXX, IMP, 7); l[0x5C] = i("???", XXX, IMP, 4); l[0x5D] = i("EOR", EOR, ABX, 4); l[0x5E] = i("LSR", LSR, ABX, 7); l[0x5F] = i("???", XXX, IMP, 7);
    l[0x60] = i("RTS", RTS, IMP, 6); l[0x61] = i("ADC", ADC, IZX, 6); l[0x62] = i("???", XXX, IMP, 2); l[0x63] = i("???", XXX, IMP, 8); l[0x64] = i("???", XXX, IMP, 3); l[0x65] = i("ADC", ADC, ZP0, 3); l[0x66] = i("ROR", ROR, ZP0, 5); l[0x67] = i("???", XXX, IMP, 5); l[0x68] = i("PLA", PLA, IMP, 4); l[0x69] = i("ADC", ADC, IMM, 2); l[0x6A] = i("ROR", ROR, IMP, 2); l[0x6B] = i("???", XXX, IMP, 2); l[0x6C] = i("JMP", JMP, IND, 5); l[0x6D] = i("ADC", ADC, ABS, 4); l[0x6E] = i("ROR", ROR, ABS, 6); l[0x6F] = i("???", XXX, IMP, 6);
    l[0x70] = i("BVS", BVS, REL, 2); l[0x71] = i("ADC", ADC, IZY, 5); l[0x72] = i("???", XXX, IMP, 2); l[0x73] = i("???", XXX, IMP, 8); l[0x74] = i("???", XXX, IMP, 4); l[0x75] = i("ADC", ADC, ZPX, 4); l[0x76] = i("ROR", ROR, ZPX, 6); l[0x77] = i("???", XXX, IMP, 6); l[0x78] = i("SEI", SEI, IMP, 2); l[0x79] = i("ADC", ADC, ABY, 4); l[0x7A] = i("???", XXX, IMP, 2); l[0x7B] = i("???", XXX, IMP, 7); l[0x7C] = i("???", XXX, IMP, 4); l[0x7D] = i("ADC", ADC, ABX, 4); l[0x7E] = i("ROR", ROR, ABX, 7); l[0x7F] = i("???", XXX, IMP, 7);
    l[0x80] = i("???", XXX, IMP, 2); l[0x81] = i("STA", STA, IZX, 6); l[0x82] = i("???", XXX, IMP, 2); l[0x83] = i("???", XXX, IMP, 6); l[0x84] = i("STY", STY, ZP0, 3); l[0x85] = i("STA", STA, ZP0, 3); l[0x86] = i("STX", STX, ZP0, 3); l[0x87] = i("???", XXX, IMP, 3); l[0x88] = i("DEY", DEY, IMP, 2); l[0x89] = i("???", XXX, IMP, 2); l[0x8A] = i("TXA", TXA, IMP, 2); l[0x8B] = i("???", XXX, IMP, 2); l[0x8C] = i("STY", STY, ABS, 4); l[0x8D] = i("STA", STA, ABS, 4); l[0x8E] = i("STX", STX, ABS, 4); l[0x8F] = i("???", XXX, IMP, 4);
    l[0x90] = i("BCC", BCC, REL, 2); l[0x91] = i("STA", STA, IZY, 6); l[0x92] = i("???", XXX, IMP, 2); l[0x93] = i("???", XXX, IMP, 6); l[0x94] = i("STY", STY, ZPX, 4); l[0x95] = i("STA", STA, ZPX, 4); l[0x96] = i("STX", STX, ZPY, 4); l[0x97] = i("???", XXX, IMP, 4); l[0x98] = i("TYA", TYA, IMP, 2); l[0x99] = i("STA", STA, ABY, 5); l[0x9A] = i("TXS", TXS, IMP, 2); l[0x9B] = i("???", XXX, IMP, 5); l[0x9C] = i("???", XXX, IMP, 4); l[0x9D] = i("STA", STA, ABX, 5); l[0x9E] = i("???", XXX, IMP, 5); l[0x9F] = i("???", XXX, IMP, 5);
    l[0xA0] = i("LDY", LDY, IMM, 2); l[0xA1] = i("LDA", LDA, IZX, 6); l[0xA2] = i("LDX", LDX, IMM, 2); l[0xA3] = i("???", XXX, IMP, 2); l[0xA4] = i("LDY", LDY, ZP0, 3); l[0xA5] = i("LDA", LDA, ZP0, 3); l[0xA6] = i("LDX", LDX, ZP0, 3); l[0xA7] = i("???", XXX, IMP, 3); l[0xA8] = i("TAY", TAY, IMP, 2); l[0xA9] = i("LDA", LDA, IMM, 2); l[0xAA] = i("TAX", TAX, IMP, 2); l[0xAB] = i("???", XXX, IMP, 2); l[0xAC] = i("LDY", LDY, ABS, 4); l[0xAD] = i("LDA", LDA, ABS, 4); l[0xAE] = i("LDX", LDX, ABS, 4); l[0xAF] = i("???", XXX, IMP, 4);
    l[0xB0] = i("BCS", BCS, REL, 2); l[0xB1] = i("LDA", LDA, IZY, 5); l[0xB2] = i("???", XXX, IMP, 2); l[0xB3] = i("???", XXX, IMP, 5); l[0xB4] = i("LDY", LDY, ZPX, 4); l[0xB5] = i("LDA", LDA, ZPX, 4); l[0xB6] = i("LDX", LDX, ZPY, 4); l[0xB7] = i("???", XXX, IMP, 4); l[0xB8] = i("CLV", CLV, IMP, 2); l[0xB9] = i("LDA", LDA, ABY, 4); l[0xBA] = i("TSX", TSX, IMP, 2); l[0xBB] = i("???", XXX, IMP, 4); l[0xBC] = i("LDY", LDY, ABX, 4); l[0xBD] = i("LDA", LDA, ABX, 4); l[0xBE] = i("LDX", LDX, ABY, 4); l[0xBF] = i("???", XXX, IMP, 4);
    l[0xC0] = i("CPY", CPY, IMM, 2); l[0xC1] = i("CMP", CMP, IZX, 6); l[0xC2] = i("???", XXX, IMP, 2); l[0xC3] = i("???", XXX, IMP, 8); l[0xC4] = i("CPY", CPY, ZP0, 3); l[0xC5] = i("CMP", CMP, ZP0, 3); l[0xC6] = i("DEC", DEC, ZP0, 5); l[0xC7] = i("???", XXX, IMP, 5); l[0xC8] = i("INY", INY, IMP, 2); l[0xC9] = i("CMP", CMP, IMM, 2); l[0xCA] = i("DEX", DEX, IMP, 2); l[0xCB] = i("???", XXX, IMP, 2); l[0xCC] = i("CPY", CPY, ABS, 4); l[0xCD] = i("CMP", CMP, ABS, 4); l[0xCE] = i("DEC", DEC, ABS, 6); l[0xCF] = i("???", XXX, IMP, 6);
    l[0xD0] = i("BNE", BNE, REL, 2); l[0xD1] = i("CMP", CMP, IZY, 5); l[0xD2] = i("???", XXX, IMP, 2); l[0xD3] = i("???", XXX, IMP, 5); l[0xD4] = i("???", XXX, IMP, 4); l[0xD5] = i("CMP", CMP, ZPX, 4); l[0xD6] = i("DEC", DEC, ZPX, 6); l[0xD7] = i("???", XXX, IMP, 6); l[0xD8] = i("CLD", CLD, IMP, 2); l[0xD9] = i("CMP", CMP, ABY, 4); l[0xDA] = i("NOP", NOP, IMP, 2); l[0xDB] = i("???", XXX, IMP, 7); l[0xDC] = i("???", XXX, IMP, 7); l[0xDD] = i("CMP", CMP, ABX, 4); l[0xDE] = i("DEC", DEC, ABX, 7); l[0xDF] = i("???", XXX, IMP, 7);
    l[0xE0] = i("CPX", CPX, IMM, 2); l[0xE1] = i("SBC", SBC, IZX, 6); l[0xE2] = i("???", XXX, IMP, 2); l[0xE3] = i("???", XXX, IMP, 8); l[0xE4] = i("CPX", CPX, ZP0, 3); l[0xE5] = i("SBC", SBC, ZP0, 3); l[0xE6] = i("INC", INC, ZP0, 5); l[0xE7] = i("???", XXX, IMP, 5); l[0xE8] = i("INX", INX, IMP, 2); l[0xE9] = i("SBC", SBC, IMM, 2); l[0xEA] = i("NOP", NOP, IMP, 2); l[0xEB] = i("???", XXX, IMP, 2); l[0xEC] = i("CPX", CPX, ABS, 4); l[0xED] = i("SBC", SBC, ABS, 4); l[0xEE] = i("INC", INC, ABS, 6); l[0xEF] = i("???", XXX, IMP, 6);
    l[0xF0] = i("BEQ", BEQ, REL, 2); l[0xF1] = i("SBC", SBC, IZY, 5); l[0xF2] = i("???", XXX, IMP, 2); l[0xF3] = i("???", XXX, IMP, 5); l[0xF4] = i("???", XXX, IMP, 4); l[0xF5] = i("SBC", SBC, ZPX, 4); l[0xF6] = i("INC", INC, ZPX, 6); l[0xF7] = i("???", XXX, IMP, 6); l[0xF8] = i("SED", SED, IMP, 2); l[0xF9] = i("SBC", SBC, ABY, 4); l[0xFA] = i("NOP", NOP, IMP, 2); l[0xFB] = i("???", XXX, IMP, 7); l[0xFC] = i("???", XXX, IMP, 7); l[0xFD] = i("SBC", SBC, ABX, 4); l[0xFE] = i("INC", INC, ABX, 7); l[0xFF] = i("???", XXX, IMP, 7);
}

void shutdown(CPU6502 &cpu) {

}

bool connect_bus(CPU6502 &cpu, Bus &bus) {
    cpu.bus = &bus;
    bus.cpu = &cpu;
    return true;
}

void write(CPU6502 &cpu, u16 address, u8 value) {
    if (!cpu.bus) {
        log_debug("CPU trying to write to null bus");
        return;
    }

    cpu_write(*cpu.bus, address, value);
}

u8 read(CPU6502 &cpu, u16 address) {
    if (!cpu.bus) {
        log_debug("CPU trying to read from null bus");
        return 0;
    }

    return cpu_read(*cpu.bus, address, false);
}

u8   get_flag(CPU6502 &cpu, CPU6502::Flag flag) {
    return ((cpu.status & flag) > 0) ? 1 : 0;
}

void set_flag(CPU6502 &cpu, CPU6502::Flag flag, bool value) {
    if (value)  cpu.status |= flag;
    else        cpu.status &= ~flag;
}

void signal_clock(CPU6502 &cpu) {
    if (cpu.cycles_left_for_this_inst == 0) {
        cpu.current_opcode = read(cpu, cpu.pc);
        cpu.pc++;

        cpu.current_inst = cpu.instructions_lookup[cpu.current_opcode];

        cpu.cycles_left_for_this_inst = cpu.current_inst.cycle_cost;

        u8 addressing_mode_requires_additional_cycle = 0;
        switch(cpu.current_inst.address_mode) {
            case IMP: {
                cpu.last_fetched = cpu.a;
            } break;

            case IMM: {
                cpu.current_absolute_address = cpu.pc;
                cpu.pc++;
            } break;

            case ZP0: {
                cpu.current_absolute_address = read(cpu, cpu.pc);
                cpu.pc++;
                cpu.current_absolute_address &= 0x00FF;
            } break;

            case ZPX: {
                cpu.current_absolute_address = read(cpu, cpu.pc) + cpu.x;
                cpu.pc++;
                cpu.current_absolute_address &= 0x00FF;
            } break;

            case ZPY: {
                cpu.current_absolute_address = read(cpu, cpu.pc) + cpu.y;
                cpu.pc++;
                cpu.current_absolute_address &= 0x00FF;
            } break;

            case ABS: {
                u8 lo = read(cpu, cpu.pc);
                cpu.pc++;
                u8 hi = read(cpu, cpu.pc);
                cpu.pc++;

                cpu.current_absolute_address = (hi << 8) | lo;
            } break;

            case ABX: {
                u8 lo = read(cpu, cpu.pc);
                cpu.pc++;
                u8 hi = read(cpu, cpu.pc);
                cpu.pc++;

                cpu.current_absolute_address = (hi << 8) | lo;
                cpu.current_absolute_address += cpu.x;

                if ((cpu.current_absolute_address & 0xFF00) != (hi << 8)) {
                    addressing_mode_requires_additional_cycle = 1;
                }
            } break;

            case ABY: {
                u8 lo = read(cpu, cpu.pc);
                cpu.pc++;
                u8 hi = read(cpu, cpu.pc);
                cpu.pc++;

                cpu.current_absolute_address = (hi << 8) | lo;
                cpu.current_absolute_address += cpu.y;

                if ((cpu.current_absolute_address & 0xFF00) != (hi << 8)) {
                    addressing_mode_requires_additional_cycle = 1;
                }
            } break;

            case IND: {
                u16 ptr_lo = read(cpu, cpu.pc);
                cpu.pc++;
                u16 ptr_hi = read(cpu, cpu.pc);
                cpu.pc++;

                u16 ptr = (ptr_hi << 8) | ptr_lo;

                if (ptr_lo == 0x00FF) { // Simulate page boundary wrap around, fixing a hardware bug
                    cpu.current_absolute_address = (read(cpu, ptr & 0xFF00) << 8) | read(cpu, ptr);
                }
                else { // Behave normally.
                    cpu.current_absolute_address = (read(cpu, ptr+1) << 8) | read(cpu, ptr);
                }
            } break;

            case IZX: {
                u16 t = read(cpu, cpu.pc);
                cpu.pc++;

                u16 lo = read(cpu, (t + cpu.x) & 0x00FF);
                u16 hi = read(cpu, (t + cpu.x + 1) & 0x00FF);

                cpu.current_absolute_address = (hi << 8) | lo;
            } break;

            case IZY: {
                u16 t = read(cpu, cpu.pc);
                cpu.pc++;

                u16 lo = read(cpu, (t) & 0x00FF);
                u16 hi = read(cpu, (t + 1) & 0x00FF);

                cpu.current_absolute_address = (hi << 8) | lo;
                cpu.current_absolute_address += cpu.y;

                if ((cpu.current_absolute_address & 0xFF00) != (hi << 8)) {
                    addressing_mode_requires_additional_cycle = 1;
                }
            } break;

            case REL: {
                cpu.current_relative_address = read(cpu, cpu.pc);
                cpu.pc++;

                if (cpu.current_relative_address & 0x80) {
                    // If the relative address (s8 read from memory) is signed (ie MSB is set),
                    // we set the high(est 8) bit(s) of the s16 relative address to make sure it
                    // is also negative.
                    cpu.current_relative_address = cpu.current_relative_address | 0xFF00;
                }
            } break;

            default: UNREACHABLE();
        }

        u8 operation_requires_additional_cycle = 0;
        switch(cpu.current_inst.opcode) {
            case BCS: {
                if (get_flag(cpu, CPU6502::C)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case BCC: {
                if (!get_flag(cpu, CPU6502::C)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case BEQ: {
                if (get_flag(cpu, CPU6502::Z)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case BNE: {
                if (!get_flag(cpu, CPU6502::Z)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case BMI: {
                if (get_flag(cpu, CPU6502::N)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case BPL: {
                if (!get_flag(cpu, CPU6502::N)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case BVC: {
                if (!get_flag(cpu, CPU6502::V)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case BVS: {
                if (get_flag(cpu, CPU6502::V)) {
                    cpu.cycles_left_for_this_inst++;

                    cpu.current_absolute_address = cpu.pc + cpu.current_relative_address;

                    if ((cpu.current_absolute_address & 0xFF00) != (cpu.pc & 0xFF00))  cpu.cycles_left_for_this_inst++;

                    cpu.pc = cpu.current_absolute_address;
                }
            } break;

            case CLC: set_flag(cpu, CPU6502::C, 0); break;
            case CLD: set_flag(cpu, CPU6502::D, 0); break;
            case CLI: set_flag(cpu, CPU6502::I, 0); break;
            case CLV: set_flag(cpu, CPU6502::V, 0); break;
            case SEC: set_flag(cpu, CPU6502::C, 1); break;
            case SED: set_flag(cpu, CPU6502::D, 1); break;
            case SEI: set_flag(cpu, CPU6502::I, 1); break;

            case STA: write(cpu, cpu.current_absolute_address, cpu.a); break;
            case STX: write(cpu, cpu.current_absolute_address, cpu.x); break;
            case STY: write(cpu, cpu.current_absolute_address, cpu.y); break;


            case AND: {
                fetch(cpu);
                cpu.a = cpu.a & cpu.last_fetched;
                set_flag(cpu, CPU6502::Z, cpu.a == 0x00);
                set_flag(cpu, CPU6502::N, cpu.a & 0x80);

                operation_requires_additional_cycle = 1;
            } break;

            case ADC: {
                fetch(cpu);

                u16 x = (u16)cpu.a + (u16)cpu.last_fetched + (u16)get_flag(cpu, CPU6502::C);

                set_flag(cpu, CPU6502::C, x > 255);
                set_flag(cpu, CPU6502::Z, (x & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, (x & 0x0080));
                set_flag(cpu, CPU6502::V, ((~((u16)cpu.a ^ (u16)cpu.last_fetched) & ((u16)cpu.a ^ (u16)x)) & 0x0080));

                cpu.a = x & 0x00FF;

                operation_requires_additional_cycle = 1;
            } break;

            case SBC: {
                fetch(cpu);

                u16 last_fetched_inverted = (u16)cpu.last_fetched ^ 0x00FF;
                u16 x = (u16)cpu.a + (u16)last_fetched_inverted + (u16)get_flag(cpu, CPU6502::C);

                if (x & 0xFF00)        set_flag(cpu, CPU6502::C, 1);
                else                   set_flag(cpu, CPU6502::C, 0);

                if ((x & 0x00FF) == 0) set_flag(cpu, CPU6502::Z, 1);
                else                   set_flag(cpu, CPU6502::Z, 0);

                if (x & 0x80)          set_flag(cpu, CPU6502::N, 1);
                else                   set_flag(cpu, CPU6502::N, 0);

                if ((x ^ (u16)cpu.a) & (x ^ last_fetched_inverted) & 0x0080) set_flag(cpu, CPU6502::V, 1);
                else                                                         set_flag(cpu, CPU6502::V, 0);

                cpu.a = x & 0x00FF;

                operation_requires_additional_cycle = 1;
            } break;

            case ASL: {
                fetch(cpu);
                auto temp = (u16)cpu.last_fetched << 1;

                set_flag(cpu, CPU6502::C, (temp & 0xFF00) > 0);
                set_flag(cpu, CPU6502::Z, (temp & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, (temp & 0x0080));

                if (cpu.instructions_lookup[cpu.current_opcode].address_mode == IMP) {
                    cpu.a = temp & 0x00FF;
                } else {
                    write(cpu, cpu.current_absolute_address, temp & 0x00FF);
                }
            } break;

            case BIT: {
                fetch(cpu);
                auto temp = (u16)cpu.a & (u16)cpu.last_fetched;

                set_flag(cpu, CPU6502::Z, (temp & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, cpu.last_fetched & (1 << 7));
                set_flag(cpu, CPU6502::V, cpu.last_fetched & (1 << 6));
            } break;

            case CMP: {
                fetch(cpu);

                set_flag(cpu, CPU6502::C, cpu.a >= cpu.last_fetched);

                auto temp = (u16)cpu.a - (u16)cpu.last_fetched;
                set_flag(cpu, CPU6502::Z, (temp & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, temp & 0x0080);
            } break;

            case CPX: {
                fetch(cpu);

                set_flag(cpu, CPU6502::C, cpu.x >= cpu.last_fetched);

                auto temp = (u16)cpu.x - (u16)cpu.last_fetched;
                set_flag(cpu, CPU6502::Z, (temp & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, temp & 0x0080);
            } break;

            case CPY: {
                fetch(cpu);

                set_flag(cpu, CPU6502::C, cpu.y >= cpu.last_fetched);

                auto temp = (u16)cpu.y - (u16)cpu.last_fetched;
                set_flag(cpu, CPU6502::Z, (temp & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, temp & 0x0080);
            } break;

            case DEC: {
                fetch(cpu);

                auto temp = cpu.last_fetched - 1;

                write(cpu, cpu.current_absolute_address, temp & 0x00FF);

                set_flag(cpu, CPU6502::Z, (temp & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, temp & 0x0080);
            } break;

            case DEX: {
                cpu.x--;
                set_flag(cpu, CPU6502::Z, cpu.x == 0x00);
                set_flag(cpu, CPU6502::N, cpu.x & 0x80);
            } break;

            case DEY: {
                cpu.y--;
                set_flag(cpu, CPU6502::Z, cpu.y == 0x00);
                set_flag(cpu, CPU6502::N, cpu.y & 0x80);
            } break;

            case EOR: {
                fetch(cpu);
                cpu.a = cpu.a ^ cpu.last_fetched;
                set_flag(cpu, CPU6502::Z, cpu.a == 0x00);
                set_flag(cpu, CPU6502::N, cpu.a & 0x80);
                operation_requires_additional_cycle = 1;
            } break;

            case ORA: {
                fetch(cpu);

                cpu.a = cpu.a | cpu.last_fetched;

                set_flag(cpu, CPU6502::Z, cpu.a == 0x00);
                set_flag(cpu, CPU6502::N, cpu.a & 0x80);
                operation_requires_additional_cycle = 1;
            } break;

            case INC: {
                fetch(cpu);

                auto temp = cpu.last_fetched + 1;

                write(cpu, cpu.current_absolute_address, temp & 0x00FF);

                set_flag(cpu, CPU6502::Z, (temp & 0x00FF) == 0);
                set_flag(cpu, CPU6502::N, temp & 0x0080);
            } break;

            case INX: {
                cpu.x++;
                set_flag(cpu, CPU6502::Z, cpu.x == 0x00);
                set_flag(cpu, CPU6502::N, cpu.x & 0x80);
            } break;

            case INY: {
                cpu.y++;
                set_flag(cpu, CPU6502::Z, cpu.y == 0x00);
                set_flag(cpu, CPU6502::N, cpu.y & 0x80);
            } break;

            case BRK: {
                cpu.pc++;

                set_flag(cpu, CPU6502::I, 1);

                write(cpu, 0x0100 + cpu.sp, (cpu.pc >> 8) & 0x00FF);  cpu.sp--;
                write(cpu, 0x0100 + cpu.sp, cpu.pc & 0x00FF);         cpu.sp--;

                set_flag(cpu, CPU6502::B, 1);
                write(cpu, 0x0100 + cpu.sp, cpu.status);              cpu.sp--;
                set_flag(cpu, CPU6502::B, 0);

                cpu.pc = (u16)read(cpu, 0xFFFE) | ((u16)read(cpu, 0xFFFF) << 8);
            } break;

            case PHA: {
                // Push onto the stack
                write(cpu, 0x0100 + cpu.sp, cpu.a);
                cpu.sp--;
            } break;

            case PHP: {
                write(cpu, 0x0100 + cpu.sp, cpu.status | CPU6502::B | CPU6502::U);
                cpu.sp--;
                set_flag(cpu, CPU6502::B, 0);
                set_flag(cpu, CPU6502::U, 0);
            } break;

            case PLA: {
                // Pull from the stack
                cpu.sp++;
                cpu.a = read(cpu, 0x0100 + cpu.sp);

                if (cpu.a == 0) set_flag(cpu, CPU6502::Z, 1);
                else            set_flag(cpu, CPU6502::Z, 0);

                if (cpu.a & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);
            } break;

            case PLP: {
                // Pull from the stack
                cpu.sp++;
                cpu.status = read(cpu, 0x0100 + cpu.sp);

                set_flag(cpu, CPU6502::U, 1);
            } break;

            case ROL: {
                fetch(cpu);
                auto temp = (u16)(cpu.last_fetched << 1) | (u16)get_flag(cpu, CPU6502::C);

                if (temp & 0xFF00)        set_flag(cpu, CPU6502::C, 1);
                else                      set_flag(cpu, CPU6502::C, 0);

                if ((temp & 0x00FF) == 0) set_flag(cpu, CPU6502::Z, 0);
                else                      set_flag(cpu, CPU6502::Z, 1);

                if (temp & 0x0080)        set_flag(cpu, CPU6502::N, 1);
                else                      set_flag(cpu, CPU6502::N, 0);

                if (cpu.instructions_lookup[cpu.current_opcode].address_mode == IMP) {
                    cpu.a = temp & 0x00FF;
                } else {
                    write(cpu, cpu.current_absolute_address, temp & 0x00FF);
                }
            } break;

            case ROR: {
                fetch(cpu);
                auto temp = (u16)(get_flag(cpu, CPU6502::C) << 7) | (u16)(cpu.last_fetched >> 1);

                if (temp & 0xFF00)        set_flag(cpu, CPU6502::C, 1);
                else                      set_flag(cpu, CPU6502::C, 0);

                if ((temp & 0x00FF) == 0) set_flag(cpu, CPU6502::Z, 0);
                else                      set_flag(cpu, CPU6502::Z, 1);

                if (temp & 0x0080)        set_flag(cpu, CPU6502::N, 1);
                else                      set_flag(cpu, CPU6502::N, 0);

                if (cpu.instructions_lookup[cpu.current_opcode].address_mode == IMP) {
                    cpu.a = temp & 0x00FF;
                } else {
                    write(cpu, cpu.current_absolute_address, temp & 0x00FF);
                }
            } break;

            case RTI: {
                // Return from an interrupt.
                cpu.sp++;
                cpu.status = read(cpu, 0x0100 + cpu.sp);
                cpu.status &= ~CPU6502::B;
                cpu.status &= ~CPU6502::U;

                cpu.sp++;
                cpu.pc = read(cpu, 0x0100 + cpu.sp);
                cpu.sp++;
                cpu.pc |= read(cpu, 0x0100 + cpu.sp) << 8;
            } break;

            case RTS: {
                cpu.sp++;
                cpu.pc = (u16)read(cpu, 0x0100 + cpu.sp);
                cpu.sp++;
                cpu.pc |= (u16)read(cpu, 0x0100 + cpu.sp) << 8;
                cpu.pc++;
            } break;

            case JMP: {
                cpu.pc = cpu.current_absolute_address;
            } break;

            case JSR: {
                cpu.pc--;

                write(cpu, 0x0100 + cpu.sp, (cpu.pc >> 8) & 0x00FF);  cpu.sp--;
                write(cpu, 0x0100 + cpu.sp, cpu.pc & 0x00FF);         cpu.sp--;

                cpu.pc = cpu.current_absolute_address;
            } break;

            case LDA: {
                cpu.a = fetch(cpu);

                if (cpu.a == 0) set_flag(cpu, CPU6502::Z, 1);
                else            set_flag(cpu, CPU6502::Z, 0);

                if (cpu.a & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);

                operation_requires_additional_cycle = 1;
            } break;

            case LDX: {
                cpu.x = fetch(cpu);

                set_flag(cpu, CPU6502::Z, (cpu.x == 0));
                set_flag(cpu, CPU6502::N, (cpu.x & 0x80));

                operation_requires_additional_cycle = 1;
            } break;

            case LDY: {
                cpu.y = fetch(cpu);

                if (cpu.y == 0) set_flag(cpu, CPU6502::Z, 1);
                else            set_flag(cpu, CPU6502::Z, 0);

                if (cpu.y & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);

                operation_requires_additional_cycle = 1;
            } break;

            case LSR: {
                fetch(cpu);

                if (cpu.last_fetched & 0x01) set_flag(cpu, CPU6502::C, 1);
                else                     set_flag(cpu, CPU6502::C, 0);

                auto temp = cpu.last_fetched >> 1;

                if ((temp & 0x00FF) == 0x0000)   set_flag(cpu, CPU6502::Z, 1);
                else                             set_flag(cpu, CPU6502::Z, 0);

                if (temp & 0x80)                 set_flag(cpu, CPU6502::N, 1);
                else                             set_flag(cpu, CPU6502::N, 0);

                if (cpu.instructions_lookup[cpu.current_opcode].address_mode == IMP) {
                    cpu.a = temp & 0x00FF;
                } else {
                    write(cpu, cpu.current_absolute_address, temp & 0x00FF);
                }
            } break;

            case NOP: {
                // Illegal opcodes go here.
                // Based on https://wiki.nesdev.com/w/index.php/CPU_unofficial_opcodes

                switch (cpu.current_opcode) {
                case 0x1C:
                case 0x3C:
                case 0x5C:
                case 0x7C:
                case 0xDC:
                case 0xFC:
                    operation_requires_additional_cycle = 1;
                    break;
                }
            } break;

            case TAX: {
                cpu.x = cpu.a;

                if (cpu.x == 0)   set_flag(cpu, CPU6502::Z, 1);
                else              set_flag(cpu, CPU6502::Z, 0);

                if (cpu.x & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);
            } break;

            case TAY: {
                cpu.y = cpu.a;

                if (cpu.y == 0)   set_flag(cpu, CPU6502::Z, 1);
                else              set_flag(cpu, CPU6502::Z, 0);

                if (cpu.y & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);
            } break;

            case TSX: {
                cpu.x = cpu.sp;

                if (cpu.x == 0)   set_flag(cpu, CPU6502::Z, 1);
                else              set_flag(cpu, CPU6502::Z, 0);

                if (cpu.x & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);
            } break;

            case TXA: {
                cpu.a = cpu.x;

                if (cpu.a == 0)   set_flag(cpu, CPU6502::Z, 1);
                else              set_flag(cpu, CPU6502::Z, 0);

                if (cpu.a & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);
            } break;

            case TXS: {
                cpu.sp = cpu.x;
            } break;

            case TYA: {
                cpu.a = cpu.y;

                if (cpu.a == 0)   set_flag(cpu, CPU6502::Z, 1);
                else              set_flag(cpu, CPU6502::Z, 0);

                if (cpu.a & 0x80) set_flag(cpu, CPU6502::N, 1);
                else              set_flag(cpu, CPU6502::N, 0);
            } break;

            case XXX: {
                log_abort("Illegal instruction: %02X\n", cpu.current_inst.opcode);
            } break;

            default: UNREACHABLE();
        }

        cpu.cycles_left_for_this_inst += addressing_mode_requires_additional_cycle & operation_requires_additional_cycle;

        set_flag(cpu, CPU6502::U, 1);
    }

    cpu.cycles_left_for_this_inst--;
}

void signal_reset(CPU6502 &cpu) {
    u16 lo = read(cpu, 0xFFFC);
    u16 hi = read(cpu, 0xFFFC + 1);

    cpu.pc = (hi << 8) | lo;

    cpu.a = 0;
    cpu.x = 0;
    cpu.y = 0;

    cpu.sp = 0xFD;

    cpu.status = 0x00 | CPU6502::U;

    cpu.current_absolute_address = 0x0000;
    cpu.current_relative_address = 0x0000;
    cpu.last_fetched = 0x00;

    cpu.cycles_left_for_this_inst = 8;
}

void signal_interrupt_request(CPU6502 &cpu) {
    // Can be ignored if the I flag is set.
    if (get_flag(cpu, CPU6502::I)) return;

    // Write the program counter to the stack.
    // The PC is 2 bytes long so this takes 2 writes to the stack.
    write(cpu, 0x0100 + cpu.sp, (cpu.pc >> 8) & 0x00FF);
    cpu.sp--;
    write(cpu, 0x0100 + cpu.sp, cpu.pc & 0x00FF);
    cpu.sp--;

    set_flag(cpu, CPU6502::B, 0);
    set_flag(cpu, CPU6502::U, 1);
    set_flag(cpu, CPU6502::I, 1);

    // Write the status register to the stack.
    write(cpu, 0x0000 + cpu.sp, cpu.status);
    cpu.sp--;

    // Check this address for the new value of the PC.
    cpu.current_absolute_address = 0xFFFE;
    u16 lo = read(cpu, cpu.current_absolute_address);
    u16 hi = read(cpu, cpu.current_absolute_address + 1);
    cpu.pc = (hi << 8) | lo;

    cpu.cycles_left_for_this_inst = 7;
}

void signal_non_maskable_interrupt_request(CPU6502 &cpu) {
    // Exact same as signal_interrupt_request, but nothing can stop me.

    write(cpu, 0x0100 + cpu.sp, (cpu.pc >> 8) & 0x00FF);
    cpu.sp--;
    write(cpu, 0x0100 + cpu.sp, cpu.pc & 0x00FF);
    cpu.sp--;

    set_flag(cpu, CPU6502::B, 0);
    set_flag(cpu, CPU6502::U, 1);
    set_flag(cpu, CPU6502::I, 1);

    write(cpu, 0x0000 + cpu.sp, cpu.status);
    cpu.sp--;

    cpu.current_absolute_address = 0xFFFA;

    u16 lo = read(cpu, cpu.current_absolute_address);
    u16 hi = read(cpu, cpu.current_absolute_address + 1);

    cpu.pc = (hi << 8) | lo;

    cpu.cycles_left_for_this_inst = 8;
}

bool is_instruction_complete(CPU6502 &cpu)
{
    return cpu.cycles_left_for_this_inst == 0;
}
