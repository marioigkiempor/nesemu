
#pragma once

#ifndef H_6502_H_
#define H_6502_H_

#include <string>

#include "bus.h"

enum Address_Mode
{
    Unknown = 0,

    IMP, // Implied
    IMM, // Immediate
    ZP0, // Zero Page
    ZPX, // Zero Page X
    ZPY, // Zero Page Y
    REL, // Relative
    ABS, // Absolute
    ABX, // Absolute X
    ABY, // Absolute Y
    IND, // Indirect
    IZX, // Indirect X
    IZY, // Indirect Y
};

enum Opcode
{
    XXX = 0,

    ADC, // Add memory to accumulator with carry
    AND, // AND memory with accumulator
    ASL, // Shift left one bit (memory or accumulator)

    BCC, // Branch if carry clear
    BCS, // Branch if carry set
    BEQ, // Branch if equal
    BIT, // Test bits in memory with accumulator
    BMI, // Branch if negative
    BNE, // Branch if not equal
    BPL, // Branch if positive
    BRK, // Force break
    BVC, // Branch if overflow clear
    BVS, // Branch if overflow set

    CLC, // Clear carry flag
    CLD, // Clear decimal mode
    CLI, // Clear interrupt disable bit
    CLV, // Clear overflow flag
    CMP, // Compare memory and accumulator
    CPX, // Compare memory and X register
    CPY, // Compare memory and Y register

    DEC, // Decrement memory by one
    DEX, // Decrement X register by one
    DEY, // Decrement Y register by one

    EOR, // Exclusive OR memory with accumulator

    INC, // Increment memory by one
    INX, // Increment X register by one
    INY, // Increment Y register by one

    JMP, // Jump to new location
    JSR, // Jump to new location saving return address

    LDA, // Load accumulator with memory
    LDX, // Load X register with memory
    LDY, // Load Y register with memory
    LSR, // Shift right one bit (memory or accumulator)

    NOP, // No operation

    ORA, // OR memory with accumulator

    PHA, // Push accumulator on stack
    PHP, // Push processor status on stack
    PLA, // Pull accumulator from stack
    PLP, // Pull processor status from stack

    ROL, // Rotate one bit left (memory or accumulator)
    ROR, // Rotate one bit right (memory or accumulator)
    RTI, // Return from interrupt
    RTS, // Return from subroutine

    SBC, // Subtract memory from accumulator with borrow
    SEC, // Set carry flag
    SED, // Set decimal mode
    SEI, // Set interrupt disable status
    STA, // Store accumulator in memory
    STX, // Store X register in memory
    STY, // Store Y register in memory

    TAX, // Transfer accumulator to X register
    TAY, // Transfer accumulator to Y register
    TSX, // Transfer stack pointer to X register
    TXA, // Transfer X register to accumulator
    TXS, // Transfer X register to stack pointer
    TYA, // Transfer Y register to accumulator
};

struct Bus;

struct CPU6502 {
    enum Flag {
        C = (0x01 << 0),     // Carry
        Z = (0x01 << 1),     // Zero
        I = (0x01 << 2),     // Interrupt Disable
        D = (0x01 << 3),     // Decimal Mode
        B = (0x01 << 4),     // Break
        U = (0x01 << 5),     // Unused
        V = (0x01 << 6),     // Overflow
        N = (0x01 << 7)      // Negative
    };

    struct Instruction
    {
        std::string  name;

        Opcode       opcode;
        Address_Mode address_mode;

        u8           cycle_cost;
    };

    u8  a; // Accumulator
    u8  x; // X Register
    u8  y; // Y Register
    u8  sp; // Stack Pointer
    u16 pc; // Program Counter
    u8  status; // Status Register

    Bus *bus;

    std::array<Instruction, 256> instructions_lookup;

    Instruction current_inst;
    u16 current_absolute_address = 0x0000; // I think this is the memory we are currently reading from
    u16 current_relative_address = 0x00;
    u8  current_opcode = 0x00;
    u8  cycles_left_for_this_inst = 0x00;

    u8 last_fetched = 0x00;
};

void init(CPU6502 &cpu);
void shutdown(CPU6502 &cpu);

bool connect_bus(CPU6502 &cpu, Bus &bus);

void write(CPU6502 &cpu, u16 address, u8 value);
u8   read (CPU6502 &cpu, u16 address);

u8   get_flag(CPU6502 &cpu, CPU6502::Flag flag);
void set_flag(CPU6502 &cpu, CPU6502::Flag flag, bool value);

void signal_clock(CPU6502 &cpu);
void signal_reset(CPU6502 &cpu);                          // The next 3 signals interrupt the CPU execution after the current instruction is finished.
void signal_interrupt_request(CPU6502 &cpu);              // Ignored if the Interrupt Disable flag is set.
void signal_non_maskable_interrupt_request(CPU6502 &cpu); // Can never be ignored.

bool is_instruction_complete(CPU6502 &cpu);

#endif // H_6502_H_
