
#pragma once

#include "defines.h"

enum Mapper_Type {
   MAPPER_0,
};

struct Mapper {
    Mapper_Type type;

    u8 _program_bank_count;
    u8 _character_bank_count;
};

void init(Mapper *mapper, Mapper_Type type, u8 program_bank_count, u8 character_bank_count);

bool cpu_map_read (Mapper &mapper, u16 address, u32 &out_mapped_address);
bool cpu_map_write(Mapper &mapper, u16 address, u32 &out_mapped_address);
bool ppu_map_read (Mapper &mapper, u16 address, u32 &out_mapped_address);
bool ppu_map_write(Mapper &mapper, u16 address, u32 &out_mapped_address);