
#include "bus.h"
#include "cartridge.h"

void init(Bus &bus) {
    for(auto &byte : bus.cpu_ram)  byte = 0;

    connect_bus(*bus.cpu, bus);
}

void shutdown(Bus &bus) {
}

void cpu_write(Bus &bus, u16 address, u8 value) {
  bool cartridge_handled_the_write = cartridge_cpu_write(*bus.cartridge, address, value);
  if (cartridge_handled_the_write) return;

  if (address >= 0x0000 || address <= 0x1FFF) {
    bus.cpu_ram[address & 0x07FF] = value;
  }
  else if (address >= 0x2000 && address <= 0x3FFF) {
    ASSERT(bus.ppu, "cpu_write(bus): PPU is not connected");
    cpu_write(*bus.ppu, address & 0x0007, value);
  }
  else {
    log_debug("Writing to invalid address: 0x%04X", address);
    return;
  }
}

u8 cpu_read(Bus &bus, u16 address, bool read_only) {
  bool cartridge_handled_the_read = cartridge_cpu_read(*bus.cartridge, address, read_only);
  if (cartridge_handled_the_read) return 0;

  if (address >= 0x0000 && address <= 0x1FFF) {
    return bus.cpu_ram[address & 0x07FF]; // The CPU RAM is mirrored in the first 0x800 bytes.
  }
  else if (address >= 0x2000 && address <= 0x3FFF) {
    ASSERT(bus.ppu, "cpu_read(bus): PPU is not connected");
    return cpu_read(*bus.ppu, address & 0x0007, read_only);
  }
  else {
    log_debug("Reading from invalid address: 0x%04X", address);
    return 0; // @Todo: better default value
  }
}

void insert_cartridge(Bus &bus, Cartridge *cartridge) {
  bus.cartridge = cartridge;
  connect_cartridge(*bus.ppu, cartridge);
}

void reset(Bus &bus) {
    signal_reset(*bus.cpu);
    bus._system_clock_count = 0;
}
void clock(Bus &bus) {

}
