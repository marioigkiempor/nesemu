
#include "mapper.h"

void init(Mapper *mapper, Mapper_Type type, u8 program_bank_count, u8 character_bank_count) {
    mapper->type = type;
    mapper->_program_bank_count = program_bank_count;
    mapper->_character_bank_count = character_bank_count;
}

bool cpu_map_read (Mapper &mapper, u16 address, u32 &out_mapped_address) {
  switch(mapper.type) {
    case MAPPER_0: {
      if (address >= 0x8000 && address <= 0xFFFF) {
          
          return true;
      }

      return false;
    } break;

    default: log_abort("Unsupported mapper type: %d", mapper.type); break;
  }
}

bool cpu_map_write(Mapper &mapper, u16 address, u32 &out_mapped_address) {
  switch(mapper.type) {
    case MAPPER_0: {
      if (address >= 0x8000 && address <= 0xFFFF) {
          
          return true;
      }

      return false;
    } break;

    default: log_abort("Unsupported mapper type: %d", mapper.type); break;
  }
}

bool ppu_map_read (Mapper &mapper, u16 address, u32 &out_mapped_address) {
  switch(mapper.type) {
    case MAPPER_0: {

      if (address >= 0x0000 && address <= 0x1FFF) {
          
          return true;
      }

      return false;
    } break;

    default: log_abort("Unsupported mapper type: %d", mapper.type); break;
  }
}

bool ppu_map_write(Mapper &mapper, u16 address, u32 &out_mapped_address) {
  switch(mapper.type) {
    case MAPPER_0: {
      if (address >= 0x0000 && address <= 0x1FFF) {
          
          return true;
      }

      return false;

    } break;

    default: log_abort("Unsupported mapper type: %d", mapper.type); break;
  }
}