
#include <cstring>
#include <cstdlib>

#include "defines.h"

void log_format(const char* tag, const char* message, va_list args) {   printf("[%s] ", tag);  vprintf(message, args);    printf("\n"); }
// void log_format(const char* tag, const char* message, va_list args) {   time_t now;     time(&now);     char * date =ctime(&now);   date[strlen(date) - 1] = '\0';  printf("%s [%s] ", date, tag);  vprintf(message, args);     printf("\n"); }
void log_info(const char* message, ...)  {  va_list args;  va_start(args, message);  log_format("info", message, args);   va_end(args); }
void log_error(const char* message, ...) {  va_list args;  va_start(args, message);  log_format("error", message, args);  va_end(args); }
void log_debug(const char* message, ...) {  va_list args;  va_start(args, message);  log_format("debug", message, args);  va_end(args); }
void log_abort(const char* message, ...) {  va_list args;  va_start(args, message);  log_format("abort", message, args);  exit(-1);     }

// https://stackoverflow.com/a/33447587
char* hex_string(u64 w, size_t hex_len) {
    char str[128];
    str[hex_len] = '\0';
    for (size_t i = hex_len; i > 0; i--) {
        str[i - 1] = "0123456789ABCDEF"[w & 15];
        w >>= 4;
    }
    return strdup(str);
}