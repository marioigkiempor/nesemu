
#include <fstream>

#include "cartridge.h"

void init(Cartridge *cartridge, std::string filename) {
  struct Header {
    char name[4];
    u8 prg_rom_chunks;
    u8 chr_rom_chunks;
    u8 mapper_1;
    u8 mapper_2;
    u8 tv_system_1;
    u8 tv_system_2;

    char _unused[5];
  };

  Header header;

  std::ifstream file_stream;
  file_stream.open(filename, std::ifstream::binary);

  if (!file_stream.is_open()) {
    log_debug("Error initialising cartridge: Failed to open file `%s`.", filename.c_str());
    return;
  }

  file_stream.read((char *)&header, sizeof(Header));

  if (header.mapper_1 & 0x04) {
    log_debug("Error initialising cartridge: Cartridge is in trainer mode.");
    file_stream.seekg(512, std::ios::cur);
  }

  cartridge->_mapper_id = ((header.mapper_2 >> 4) << 4) | (header.mapper_1 >> 4);

  auto file_type = 1; // There are 3 file formats. @Todo: support more.
  switch(file_type) {
    case 0: log_abort("@Todo: unsupported file_type"); break;

    case 1: {
      cartridge->_program_bank_count = header.prg_rom_chunks;
      cartridge->program_memory.resize(cartridge->_program_bank_count * 4 * KiB);
      file_stream.read((char *)cartridge->program_memory.data(), cartridge->program_memory.size());

      cartridge->_character_bank_count = header.prg_rom_chunks;
      cartridge->character_memory.resize(cartridge->_character_bank_count * 8 * KiB);
      file_stream.read((char *)cartridge->character_memory.data(), cartridge->character_memory.size());
    } break;

    case 2: log_abort("@Todo: unsupported file_type"); break;

    default: log_debug("Unsupported file type: %d", file_type); break;
  }

  file_stream.close();
}

bool   cartridge_cpu_read(Cartridge &cartridge, u16 address, bool read_only) {
  return false;
}

bool cartridge_cpu_write(Cartridge &cartridge, u16 address, u8 value) {
  return false;
}

bool   cartridge_ppu_read(Cartridge &cartridge, u16 address, bool read_only) {
  return false;
}

bool cartridge_ppu_write(Cartridge &cartridge, u16 address, u8 value) {
  return false;
}