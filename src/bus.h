
#pragma once

#ifndef BUS_H_
#define BUS_H_

#include <array>

#include "defines.h"
#include "6502.h"
#include "ppu2C02.h"
#include "cartridge.h"

struct Bus {
    CPU6502* cpu;
    PPU2C02* ppu;

    // Fake RAM
    std::array<u8, 64 * KiB> cpu_ram;

    Cartridge *cartridge;

    u32 _system_clock_count;
};

void init(Bus &bus);
void shutdown(Bus &bus);

u8   cpu_read(Bus &bus, u16 address, bool read_only = false);
void cpu_write(Bus &bus, u16 address, u8 value);

// System interface for the NES.
void insert_cartridge(Bus &bus, Cartridge *cartridge);
void reset(Bus &bus);
void clock(Bus &bus);

#endif // BUS_H_
