
#include <cstdarg>
#include <cstdio>
#include <ctime>
#include <cstdint>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef float f32;
typedef double f64;

// define KiB, MiB, GiB, etc.
#define KiB 1024
#define MiB (1024 * KiB)
#define GiB (1024 * MiB)

#define UNREACHABLE() log_abort("%s:%d: unreachable", __FILE__, __LINE__)

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define CLAMP(a, min, max) MIN(MAX(a, min), max)

#define ASSERT(cond, msg) do { if (!(cond)) log_abort(msg); } while (0);

void log_error(const char* message, ...);
void log_info (const char* message, ...);
void log_debug(const char* message, ...);
void log_abort(const char* message, ...);

char* hex_string(u64 w, size_t hex_len);