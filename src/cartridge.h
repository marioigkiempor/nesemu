
#pragma once

#include <vector>

#include "defines.h"

struct Cartridge {
    std::vector<u8> program_memory;
    std::vector<u8> character_memory;

    u8 _mapper_id;
    u8 _program_bank_count;
    u8 _character_bank_count;
};

void init(Cartridge *cartridge);

bool cartridge_cpu_read(Cartridge &cartridge, u16 address, bool read_only = false);
bool cartridge_cpu_write(Cartridge &cartridge, u16 address, u8 value);

bool cartridge_ppu_read(Cartridge &cartridge, u16 address, bool read_only = false);
bool cartridge_ppu_write(Cartridge &cartridge, u16 address, u8 value);