#pragma once

#include "defines.h"

#include "cartridge.h"

struct PPU2C02 {
    Cartridge *cartridge;

    u8 _name_table[2][1 * KiB];  // NES has 2 name tables, but can address 4.
    u8 _palette_table[32];
    u8 _pattern_table[2][4 * KiB]; // Just in case Javid's code won't compile without.
};

u8   cpu_read(PPU2C02 &ppu, u16 address, bool read_only = false);
void cpu_write(PPU2C02 &ppu, u16 address, u8 value);

u8   ppu_read(PPU2C02 &ppu, u16 address, bool read_only = false);
void ppu_write(PPU2C02 &ppu, u16 address, u8 value);

void connect_cartridge(PPU2C02 &ppu, Cartridge *cartridge);
void clock(PPU2C02 &ppu);