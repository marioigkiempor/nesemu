
#include "ppu2C02.h"

u8   cpu_read(PPU2C02 &ppu, u16 address, bool read_only) {
  // The CPU can only access 8 different locations on the PPU.

  switch(address) {
  case 0x0000:   // Control
    break;
  case 0x0001:   // Mask
    break;
  case 0x0002:   // Status
    break;
  case 0x0003:   // OAM Address
    break;
  case 0x0004:   // OAM Data
    break;
  case 0x0005:   // Scroll
    break;
  case 0x0006:   // PPU Address
    break;
  case 0x0007:   // PPU Data
    break;
  default: log_abort("cpu_read(ppu): Invalid address: 0x%04X", address);
  }

  return 0; // @Todo: better default value
}

void cpu_write(PPU2C02 &ppu, u16 address, u8 value) {
  switch(address) {
  case 0x0000:   // Control
    break;
  case 0x0001:   // Mask
    break;
  case 0x0002:   // Status
    break;
  case 0x0003:   // OAM Address
    break;
  case 0x0004:   // OAM Data
    break;
  case 0x0005:   // Scroll
    break;
  case 0x0006:   // PPU Address
    break;
  case 0x0007:   // PPU Data
    break;
  default: log_abort("cpu_read(ppu): Invalid address: 0x%04X", address);
  }
}

u8   ppu_read(PPU2C02 &ppu, u16 address, bool read_only) {
  u8 data = 0x00;

  address &= 0x3FFF;

  bool cartridge_handled_the_read = cartridge_ppu_read(*ppu.cartridge, address, read_only);
  if (cartridge_handled_the_read) {
    // @Incomplete
  }

  return data;
}

void ppu_write(PPU2C02 &ppu, u16 address, u8 value) {
  address &= 0x3FFF;

  bool cartridge_handled_the_write = cartridge_ppu_write(*ppu.cartridge, address, value);
  if (cartridge_handled_the_write) {
    // @Incomplete
  }
}

void connect_cartridge(PPU2C02 &ppu, Cartridge *cartridge) {
  ppu.cartridge = cartridge;
}

void clock(PPU2C02 &ppu) {

}