
#include <iostream>
#include <iomanip>
#include <sstream>
#include <ctime>
#include <map>

#include <SDL.h>
#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_sdlrenderer.h>

#include "defines.h"
#include "bus.h"
#include "6502.h"

const int window_width  = 1200;
const int window_height = 720;

bool should_quit = false;

Bus bus;
CPU6502 cpu;

std::map<u16, std::string> disassemble(CPU6502 cpu, u16 from, u16 to) {
    std::map<u16, std::string> result;

    u32 addr = from;
    while (addr < (u32)to) {
      auto line_addr = addr; // Cache the address of the instruction.

      std::string inst_string = "$" + std::string(hex_string(addr, 4)) + ": ";

      u8 opcode = cpu_read(*cpu.bus, addr, true);      addr++;
      auto inst = cpu.instructions_lookup[opcode];

      inst_string += inst.name + " ";

      switch (inst.address_mode) {
        case IMP:
          break;

        case IMM: {
          inst_string += "#$" + std::string(hex_string(cpu_read(*cpu.bus, addr, true), 2)) + " {IMM}";
          addr++;
        } break;

        case ZP0: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = 0x00;

          inst_string += "$" + std::string(hex_string(lo, 2)) + " {ZP0}";
        } break;

        case ZPX: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = 0x00;

          inst_string += "$" + std::string(hex_string(lo, 2)) + ", X {ZPX}";
        } break;

        case ZPY: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = 0x00;

          inst_string += "$" + std::string(hex_string(lo, 2)) + ", Y {ZPY}";
        } break;

        case IZX: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = 0x00;

          inst_string += "$" + std::string(hex_string(lo, 2)) + ", X {IZX}";
        } break;

        case IZY: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = 0x00;

          inst_string += "$" + std::string(hex_string(lo, 2)) + ", Y {IZY}";
        } break;

        case ABS: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = cpu_read(*cpu.bus, addr, true);    addr++;

          inst_string += "$" + std::string(hex_string((u16)(hi<<8) | lo, 4)) + " {ABS}";
        } break;

        case ABX: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = cpu_read(*cpu.bus, addr, true);    addr++;

          inst_string += "$" + std::string(hex_string((u16)(hi<<8) | lo, 4)) + ", X {ABX}";
        } break;

        case ABY: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = cpu_read(*cpu.bus, addr, true);    addr++;

          inst_string += "$" + std::string(hex_string((u16)(hi<<8) | lo, 4)) + ", Y {ABY}";
        } break;

        case IND: {
          auto lo = cpu_read(*cpu.bus, addr, true);    addr++;
          auto hi = cpu_read(*cpu.bus, addr, true);    addr++;

          inst_string += "($" + std::string(hex_string((u16)(hi<<8) | lo, 4)) + ") {IND}";
        } break;

        case REL: {
          auto val = cpu_read(*cpu.bus, addr, true);   addr++;

          inst_string += "$" + std::string(hex_string(val, 2)) + " [$" + std::string(hex_string(val+ addr, 4)) + "] {REL}";
        } break;

        default: UNREACHABLE();
      }

      result[line_addr] = inst_string;
    }

    return result;
}

int main(int argc, char *argv[]) {
  // Initialize emulator
  init(cpu);
  connect_bus(cpu, bus);
  init(bus);

  // Load a test program into the CPU's RAM
  u8 test_program[] = { 0xA2, 0x0A, 0x8E, 0x00, 0x00, 0xA2, 0x03, 0x8E, 0x01, 0x00, 0xAC, 0x00, 0x00, 0xA9, 0x00, 0x18, 0x6D, 0x01, 0x00, 0x88, 0xD0, 0xFA, 0x8D, 0x02, 0x00, 0xEA, 0xEA, 0xEA, };
  auto offset = 0x8000;
  for (auto byte : test_program) {
    bus.cpu_ram[offset++] = byte;
  }

  // Set reset vector.
  bus.cpu_ram[0xFFFC] = 0x00;
  bus.cpu_ram[0xFFFD] = 0x80;

  // This is where you'd set the IRQ and NMI vectors, if you were using them.

  auto dissassembly = disassemble(cpu, 0x0000, 0xFFFF);

  signal_reset(cpu);

  // Initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0)  log_abort("Could not initialise SDL.");

  auto window = SDL_CreateWindow("Demo Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_width, window_height, SDL_WINDOW_OPENGL);
  if (!window)  log_abort("Could not create a window: {}", SDL_GetError());

  auto renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
  if (!renderer)  log_abort("Could not create SDL renderer: {}", SDL_GetError());

  { // Initialise ImGui
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void)io;
    // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    // io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;


    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForSDLRenderer(window, renderer);
    ImGui_ImplSDLRenderer_Init(renderer);
  }

  while (!should_quit) {
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
      ImGui_ImplSDL2_ProcessEvent(&e);

      if (e.type == SDL_QUIT) {
        should_quit = true;
        break;
      }

      else if (e.type == SDL_KEYDOWN) {
        switch (e.key.keysym.sym) {
        case SDLK_ESCAPE: {
          should_quit = true;
        } break;

        case SDLK_SPACE: {
          do {
            signal_clock(cpu);
          } while (!is_instruction_complete(cpu));

          ASSERT(cpu.cycles_left_for_this_inst == 0, "CPU did not complete instruction in the expected number of cycles.")
        } break;

        case SDLK_r: {
          signal_reset(cpu);
        } break;

        case SDLK_i: {
          signal_interrupt_request(cpu);
        } break;

        case SDLK_n: {
          signal_non_maskable_interrupt_request(cpu);
        } break;
        }
      }
    }

    ImGui_ImplSDLRenderer_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();

    {
      ImGui::Begin("Gameboy Emulator");
        ImGui::Text("Emulation information");
        if (ImGui::CollapsingHeader("CPU State", ImGuiTreeNodeFlags_DefaultOpen)) {
          auto draw_flag = [](CPU6502 cpu, CPU6502::Flag flag, const char *name) {
            if (get_flag(cpu, flag)) {
              ImGui::TextColored(ImVec4(0.f, 8.f, 0.f, 1.f), "%s", name);
            } else {
              ImGui::Text("%s", name);
            }
            ImGui::SameLine();
          };

          ImGui::Indent();

          draw_flag(cpu, CPU6502::Flag::N, "N");
          draw_flag(cpu, CPU6502::Flag::V, "V");
          draw_flag(cpu, CPU6502::Flag::C, "C");
          draw_flag(cpu, CPU6502::Flag::Z, "Z");
          draw_flag(cpu, CPU6502::Flag::I, "I");
          draw_flag(cpu, CPU6502::Flag::D, "D");
          draw_flag(cpu, CPU6502::Flag::B, "B");
          draw_flag(cpu, CPU6502::Flag::U, "U");
          ImGui::NewLine();

          ImGui::Text("Accumulator       $%02X", cpu.a);
          ImGui::Text("X Register        $%02X", cpu.x);
          ImGui::Text("Y Register        $%02X", cpu.y);
          ImGui::Text("Stack Pointer     $%02X", cpu.sp);
          ImGui::Text("Processor Status  $%02X", cpu.status);
          ImGui::Text("Program Counter   $%04X", cpu.pc);
          ImGui::Text("");
          ImGui::Text("Last inst         %s (Opcode: $%02X)", cpu.current_inst.name.c_str(), cpu.current_opcode);
          ImGui::Text("Last fetched      $%02X", cpu.last_fetched);
          ImGui::Text("Relative addr     $%02X", cpu.current_relative_address);
          ImGui::Text("Absolute addr     $%02X", cpu.current_absolute_address);

          ImGui::Unindent();
        }

        if (ImGui::CollapsingHeader("RAM", ImGuiTreeNodeFlags_DefaultOpen)) {
          // Helper function to get a R6502 RAM page as string containing a grid of bytes, given a starting address.
          auto draw_ram_page = [](Bus &bus, u16 address) {
            for (auto row = 0; row < 16; row++) {
              ImGui::Text("$%s", hex_string(address, 4));
              ImGui::SameLine();
              for (auto col = 0; col < 16; col++) {
                ImGui::Text("%s", hex_string(cpu_read(bus, address), 2));
                ImGui::SameLine();
                if ((address + 1) % 4 == 0) {
                  ImGui::Text(" ");
                  ImGui::SameLine();
                }
                address++;
              }
              ImGui::NewLine();
            }
          };

          ImGui::Indent();

          if (ImGui::CollapsingHeader("Page 0x00")) {
            ImGui::Indent();
            draw_ram_page(bus, 0x0000);
            ImGui::Unindent();
          }

          if (ImGui::CollapsingHeader("Page 0x08")) {
            ImGui::Indent();
            draw_ram_page(bus, 0x8000);
            ImGui::Unindent();
          }

          ImGui::Unindent();
        }

        if (ImGui::CollapsingHeader("Code", ImGuiTreeNodeFlags_DefaultOpen)) {
          ImGui::Indent();

          auto lines_before_current_inst = 3;
          auto lines_after_current_inst = 12;
          auto it = dissassembly.find(cpu.pc);
          if (it != dissassembly.end()) {
            for (auto i = 0; i < lines_before_current_inst; ++i) it--;
            for (auto i = 0; i < lines_before_current_inst; ++i) ImGui::Text("%s", (it++)->second.c_str());
            ImGui::TextColored(ImVec4(7.f, 8.f, 0.f, 1.f), "%s", it->second.c_str());
            for (auto i = 0; i < lines_after_current_inst; ++i) ImGui::Text("%s", (++it)->second.c_str());
          }

          ImGui::Unindent();
        }

        ImGui::Text("");

        ImGui::Text("Application information");
        ImGui::Text("Average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      ImGui::End();

      ImGui::ShowDemoWindow();
    }

    ImGui::Render();
    SDL_SetRenderDrawColor(renderer, 0x18, 0x18, 0x18, 0xFF);
    SDL_RenderClear(renderer);
    ImGui_ImplSDLRenderer_RenderDrawData(ImGui::GetDrawData());
    SDL_RenderPresent(renderer);
  }
}

