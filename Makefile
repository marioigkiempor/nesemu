# Directories
SOURCE_DIR=src
BUILD_DIR=build

SDL_DIR=thirdparty/SDL2-2.0.22
IMGUI_DIR=thirdparty/imgui
WINDOWS_SDK_DIR=C:/Program Files (x86)/Windows Kits

# Output
TARGET=gbe
EXEC=$(BUILD_DIR)/$(TARGET).exe

# Files
S_FILES=$(SOURCE_DIR)/$(TARGET).cc $(IMGUI_DIR)/backends/imgui_impl_sdl.cpp $(IMGUI_DIR)/backends/imgui_impl_sdlrenderer.cpp $(IMGUI_DIR)/*.cpp

# Build settings
CC=cl

# C flags
C_FLAGS=-std:c++latest \
-DSDL_MAIN_HANDLED \
-D_CRT_SECURE_NO_WARNINGS \
-permissive \
-EHsc \
-Zi

INCLUDES=-I$(IMGUI_DIR) \
-I$(IMGUI_DIR)/backends \
-I$(SDL_DIR)/include \
-I"$(WINDOWS_SDK_DIR)/10/Include/10.0.19041.0/shared" \
-I"$(WINDOWS_SDK_DIR)/10/Include/10.0.19041.0/um" \
-I"$(WINDOWS_SDK_DIR)/10/Include/10.0.19041.0/ucrt"

# Link flags
LINK_FLAGS=/LIBPATH:$(SDL_DIR)/lib/x64 \
/LIBPATH:"$(WINDOWS_SDK_DIR)/10/Lib/10.0.19041.0/um/x64" \
/LIBPATH:"$(WINDOWS_SDK_DIR)/10/Lib/10.0.19041.0/ucrt/x64" \
SDL2.lib SDL2main.lib
# libucrt.lib libvcruntime.lib libcmt.lib libcpmt.lib \
#  legacy_stdio_definitions.lib oldnames.lib \
#  legacy_stdio_wide_specifiers.lib \
#  kernel32.lib User32.lib

all: $(TARGET)

$(TARGET):
	$(CC) $(C_FLAGS) $(S_FILES) $(INCLUDES) -link -out:$(EXEC) $(LINK_FLAGS)

clean:
	rm -rf $(BUILD_DIR)/*
